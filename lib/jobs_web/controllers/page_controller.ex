defmodule JobsWeb.PageController do
  use JobsWeb, :controller
  import Logger

  def index(conn, params) do
    # Using a cookie for the cursor position
    # This method doesn't work when using the back button, otherwise it's good
    scroll = conn.req_cookies["scroll"] || "0" |> String.to_integer

    sort = params["sort"]
    search = params["search"] || %{}
    pattern = Map.get(search, "pattern")
    filter = Map.get(search, "filter") || ""
    filter = case String.split(filter, "valid_to=") do
      [_, _] -> filter
      _ -> filter <> " valid_to=#{Date.to_string(Date.utc_today)}"
    end

    Logger.info("index: sort by '#{inspect sort}'")
    Logger.info("index: search '#{inspect pattern}'")
    Logger.info("index: filter '#{inspect filter}'")
    jobs = Scraper.Job.get(%{sort: sort, search: pattern, filter: filter})
    render(conn, "index.html", sort: sort, jobs: jobs, scroll: scroll)
  end

  def job(conn, %{"id" => id}) do
    render(conn, "job.html", id: id)
  end
end
