defmodule Jobs.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Ecto Repo is started with Scraper
      # Start the endpoint when the application starts
      JobsWeb.Endpoint
      # Starts a worker by calling: Jobs.Worker.start_link(arg)
      # {Jobs.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Jobs.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    JobsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
