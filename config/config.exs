# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :jobs,
  ecto_repos: [Scraper.Repo]

# Configures the endpoint
config :jobs, JobsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SDy7HW45O2+G8bVc0NWtAOT4/KNPCst8+/FDJyxa7m9SojZTUaYZrmowyw/Qny1Y",
  render_errors: [view: JobsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Jobs.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
